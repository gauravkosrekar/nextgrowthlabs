For Correlation Analysis, Analysis can be found in Correlation_Analysis.ipynb .

------------

Here score represents that as score is lower, importance is high for keyword and as score is higher importance is low for keyword.

So similare rules can be followed for mean score also.

Here we can see that mean score for short description and Rank has R value 0.3876 So Short Description has Positive Correlations with Rank but not very strong but Good Correlation we can say.

Similarly we can see that mean score for long description and Rank has R value 0.1943 So Long Discription has Positive Correlations with Rank, it is also not strong.

Short Description has better correlations with Rank compare to Long Description

------------

As we can see here,

between App ID and Rank we are not seeing much correlations. While for some places App ID has more count, Rank variance and mean are behaving same with the App ID has low count.

Any trend can not be seen here.
