from flask import Flask, render_template, request, redirect, url_for
import os
from os.path import join, dirname, realpath
import pandas as pd
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer

app = Flask(__name__)

# enable debugging mode
app.config["DEBUG"] = True

# Upload folder
UPLOAD_FOLDER = '/home/gauravkosrekar/mysite/static/files'
app.config['UPLOAD_FOLDER'] =  UPLOAD_FOLDER


# Root URL
@app.route('/')
def index():
     # Set The upload HTML template '\templates\index.html'
    return render_template('index.html')

def sentiment_vader(sentence):

    # Create a SentimentIntensityAnalyzer object.
    sid_obj = SentimentIntensityAnalyzer()

    sentiment_dict = sid_obj.polarity_scores(sentence)
    negative = sentiment_dict['neg']
    neutral = sentiment_dict['neu']
    positive = sentiment_dict['pos']
    compound = sentiment_dict['compound']

    if sentiment_dict['compound'] >= 0.05 :
        overall_sentiment = "Positive"

    elif sentiment_dict['compound'] <= - 0.05 :
        overall_sentiment = "Negative"

    else :
        overall_sentiment = "Neutral"
  
    return negative, neutral, positive, compound, overall_sentiment

def identifyMismatch(row):
    '''
    Function to get dataframe row as an input and 
    check the sentiment of review text.
    Based on Sentiment of Review Text and Rating, Give the flag of Mismatch which indicates whether there is a \
    mismatch or not between review text sentiment and rating.
    '''
    review_text = row['Text']
    if (isinstance(review_text, float)):
        row['flag_Mismatch'] = False
        return row
    
    sentiment_to_rating_mapping = {
        'Neutral': [3],
        'Positive': [4,5], 
        'Negative': [1,2]
    }

    rating = row['Star']
    review_sentiment_sentiment = sentiment_vader(review_text)[4]
    
    if rating in sentiment_to_rating_mapping[review_sentiment_sentiment]:
        row['flag_Mismatch'] = False
    else:
        row['flag_Mismatch'] = True
    return row



#Process CSV
def processCSV(filePath):
    df_reviews = pd.read_csv(filePath)
    df_reviews = df_reviews.apply(lambda row: identifyMismatch(row), axis=1)
    df_reviews_mismatch = df_reviews[df_reviews.flag_Mismatch]
    return df_reviews_mismatch



# Get the uploaded files
@app.route("/", methods=['POST'])
def uploadFiles():
    # get the uploaded file
    uploaded_file = request.files['file']
    if uploaded_file.filename != '':
        file_path = os.path.join(app.config['UPLOAD_FOLDER'], uploaded_file.filename)
        # set the file path
        uploaded_file.save(file_path)
        # save the file
        df_reviews_mismatch = processCSV(file_path)
    return render_template('simple.html',  tables=[df_reviews_mismatch.to_html(classes='data', header="true")])
    # return redirect(url_for('index'))

if (__name__ == "__main__"):
     app.run(port = 5000)
