For this Problem Statement, 

First of all, I have done PoC to check the Pre Trained Model Library to check sentiments. PoC can be found in PoC.ipynb file.

After preparing Functions, API is developed in Flask. API Can be found in API folder.

Live Link: http://gauravkosrekar.pythonanywhere.com/ 

In this, You can upload csv and Get rows where mismatch is there between review text and ratings.